function s = FresnelC(x)
%FresnelC fresnel C(x) integral from matlab implementation
%   C(x) = C'(sqrt(2/pi)*x)
s = fresnelc(sqrt(2/pi).*x);
end

