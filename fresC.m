function s = fresC(x)
% fresnel C(x) integral
% = sqrt(2/pi)*int(cos(s^2),0,x)
xsign = sign(x);
s = xsign .* real(fres(x.^2));
end

