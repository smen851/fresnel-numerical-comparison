import scipy.io as io
import scipy.special as sc
import numpy as np
import sys

# this script provides an interface to calculate fresnel integrals over
# a passed matlab vector using the scipy fresnel functions
# saves the results in a matlab .mat file to be open by matlab

# fresnel funciton 
def modifiedFresnel(gamma):
    S, C = sc.fresnel(gamma * np.sqrt(2 / np.pi))
    return C + S * 1j

if __name__ == '__main__':
    
    # for testing
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))

    X_file = str(sys.argv[1]) # name of the input matlab file

    x = io.loadmat(X_file)
    print(x['x'])
    print("computing...")
    fres = modifiedFresnel(x['x'])
    io.savemat('pysnel.mat',{"pysnel":fres})
    print("saved!")
