function s = fresS(x)
% fresnel S(x) integral
% = sqrt(2/pi)*int(sin(s^2),0,x)
xsign = sign(x);
s = xsign .* (-imag(fres(x.^2)));
end

