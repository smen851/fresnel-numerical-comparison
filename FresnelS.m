function s = FresnelS(x)
%FresnelS fresnel S(x) integral from matlab implementation
%   S(x) = S'(sqrt(2/pi)*x)
s = fresnels(sqrt(2/pi).*x);
end

