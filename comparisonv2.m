% comparison between scipy, matlab and Boersma discrete fresnel integral
% implementation
clear all
close all

maxi = 10^12;
samples = 30000;
% x over wich we want to calculate fresnel integrals
%logarithmic random axis over 12 decades
x = (1000000000000.^(rand(samples,1))).*maxi/1000000000000;
x = sort(x);
Fpy = zeros(size(x));
Fboer = Fpy;
Fmat = Fpy;


tic
% save matrix to file 
save('matx.mat','x')
% get fresnel result from python
system("pysnel.py matx.mat")
load('pysnel.mat','pysnel')
tic
Fpy(:) = pysnel;
pytime = toc;
% same thing for Boersna algorithm
tic
boersnel = fresC(x) + 1i.* fresS(x);
boertime =toc;
Fboer(:) = boersnel;
% and for matlab native implementation
tic
matsnel = FresnelC(x) +1i.* FresnelS(x);
mattime = toc;
Fmat(:) = matsnel;
toc

%% Plotting

figure(1)

plot3(real(Fpy(:)),imag(Fpy(:)),x,'.')
hold on
plot3(real(Fboer(:)),imag(Fboer(:)),x,'.')
hold on
plot3(real(Fmat(:)),imag(Fmat(:)),x,'.')
hold on
legend('py', 'boer', 'mat')
set(gca, 'ZScale', 'log')
%% errors

Emp = abs(Fpy-Fmat);
Emb = abs(Fmat-Fboer);
Epb = abs(Fpy-Fboer);

figure(2)
hold on
ax1=subplot(3,1,1)
plot(x,Emb,'.','Color','#77AC30')
title('absolute difference between fresnel integral implementations')
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
ylabel('matlab vs Boersma')
ax2=subplot(3,1,3)
plot(x,Emp,'.','Color','#7E2F8E')
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
ylabel('matlab vs scipy')
xlabel('\gamma')
ax3=subplot(3,1,2)
plot(x,Epb,'.','Color','#D95319')
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
ylabel( 'scipy vs Boersma')

ylim('auto')
linkaxes([ax1 ax2 ax3],'xy')

%% max errors
disp("maximum scipy error:");
[M,I] = max(Epb);
disp(M)
disp("at argument value:")
x(I)
disp("maximum matlab error:");
[M,I]=max(Emb);
disp(M)
disp("at argument value:")
x(I)
